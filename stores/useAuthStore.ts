import { defineStore } from "pinia"
import { ref } from "vue";
import {useApiFetch} from "~/composables/useApiFetch"

type User = {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
}

type Creds = {
    email: string;
    password: string;
}

type RegistrationCreds = {
    first_name: string;
    last_name: string;
    name: string|null;
    email: string;
    password: string;
    password_confirmation: string;
}

export const useAuthStore = defineStore('auth', () => {
    const user = ref<User | null>(null)

    const isLoggedIn = computed(() => !!user.value)

    async function fetchUser() {
        const {data,error} = await useApiFetch("api/user")
        user.value = data.value as User
    }
    
    async function login(creds: Creds) {
        await useApiFetch("sanctum/csrf-cookie")
    
        const login = await useApiFetch("login", {
            method: "POST",
            body: creds,
        })

        await fetchUser()
    
        return login
    }

    async function register(creds: RegistrationCreds) {
        await useApiFetch("sanctum/csrf-cookie")

        creds.name = creds.first_name+' '+creds.last_name
    
        const register = await useApiFetch("register", {
            method: "POST",
            body: creds,
        })

        await fetchUser()
    
        return register
    }

    async function logout() {
        await useApiFetch('/logout', {method: "POST"})
        user.value = null
        navigateTo('/login')
    }

    return { user, login, isLoggedIn, fetchUser, logout, register }
  })